Prometheus role
=========

Installs Prometheus on Fedora, Debian/Ubuntu servers.

Example Playbook
----------------
```yml
- hosts: all
  become: true
  roles:
    - tyumentsev4.prometheus
```
